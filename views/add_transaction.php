<?php

$transaction_operation_label = $transaction->isNew() ? lang('add transaction') : lang('edit transaction');
set_page_title($transaction_operation_label);
project_tabbed_navigation();
project_crumbs(array(
		     array(lang('finances'), get_url('finances')),
		     array($transaction_operation_label)
		     ));

setlocale(LC_MONETARY, 'es_PA');
$locale_info = localeconv();

?>
<form action="<?php echo get_url('finances', 'add_transaction'); ?>" method="post">
  <?php tpl_display(get_template_path('form_errors')); ?>
  <?php $issued_on_string = $transaction->isNew() ? date_format(new DateTime(), 'M j, Y')  : $transaction->getIssuedOn()->format('M j, Y'); ?>
<div>
  <?php echo label_tag(lang('issued on'), 'financesFormIssuedOn', true); ?>
  <?php echo text_field('transaction[issued_on]', $issued_on_string, array('id' => 'financesFormIssuedOn', 'class' => 'datepicker')); ?>
</div>
<div>
  <?php echo label_tag(lang('description'), 'financesFormDescription', true); ?>
  <?php echo textarea_field('transaction[description]', $transaction->getDescription(), array('id' => 'financesFormDescription')); ?>
</div>
<div>
  <?php echo label_tag(lang('amount'), 'financesFormAmount', true); ?>
  <span><?php echo $locale_info['currency_symbol']; ?></span><?php echo text_field('transaction[amount]', $transaction->getAmount(), array('id' => 'financesFormAmount')); ?>
</div>
  <?php if(!$transaction->isNew()): ?>
<div>
  <a href="<?php echo $transaction->getDeleteUrl(); ?>">Delete</a>
</div>
  <?php endif; ?>
  <?php echo submit_button($transaction_operation_label); ?>
</form>