<?php

set_page_title(lang('finances'));
project_tabbed_navigation();
project_crumbs(array(
		     array(lang('finances'), get_url('finances'))
		     ));

if(FinancesTransaction::canAdd(logged_user(), active_project())) {
  add_page_action(lang('add transaction'), get_url('finances', 'add_transaction'));
}

?>
<p>Total: <?php echo $total; ?></p>
<table>
<thead>
<tr>
<th>Date</th>
<th>Description</th>
<th>Debit</th>
<th>Credit</th>
</tr>
</thead>
<tbody>
<?php foreach ($transactions as $transaction): ?>
<tr>
<td><?php echo $transaction->getIssuedOn()->format('Y-m-d H:i:s'); ?></td>
<td><a href="<?php echo $transaction->getEditUrl(); ?>"><?php echo $transaction->getDescription(); ?></a></td>
<td><?php if($transaction->getAmount() >= 0) { echo abs($transaction->getAmount()); } ?></td>
<td><?php if($transaction->getAmount() < 0) { echo abs($transaction->getAmount()); } ?></td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
