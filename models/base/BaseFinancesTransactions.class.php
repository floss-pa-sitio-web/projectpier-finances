<?php

abstract class BaseFinancesTransactions extends DataManager {
  static private $columns = array(
				  'id' => DATA_TYPE_INTEGER,
				  'project_id' => DATA_TYPE_INTEGER,
				  'description' => DATA_TYPE_STRING,
				  'amount' => DATA_TYPE_FLOAT,
				  'issued_on' => DATA_TYPE_DATETIME,
				  'created_on' => DATA_TYPE_DATETIME,
				  'created_by_id' => DATA_TYPE_INTEGER
				  );

  function __construct() {
    parent::__construct('FinancesTransaction', 'project_finances', true);
  }

  function getColumns() {
    return array_keys(self::$columns);
  }

  function getColumnType($column_name) {
    return self::$columns[$column_name];
  }

  function getPkColumns() {
    return 'id';
  }
  
  function getAutoIncrementColumn() {
    return 'id';
  }

  function find($arguments = null) {
    if (isset($this) && instance_of($this, 'FinancesTransactions')) {
      return parent::find($arguments);
    } else {
      return FinancesTransactions::instance()->find($arguments);
    }
  }

  function findAll($arguments = null) {
    if (isset($this) && instance_of($this, 'FinancesTransactions')) {
      return parent::findAll($arguments);
    } else {
      return FinancesTransactions::instance()->findAll($arguments);
    }
  }

  function findOne($arguments = null) {
    if (isset($this) && instance_of($this, 'FinancesTransactions')) {
      return parent::findOne($arguments);
    } else {
      return FinancesTransactions::instance()->findOne($arguments);
    }
  }

  function findById($id, $force_reload = false) {
    if (isset($this) && instance_of($this, 'FinancesTransactions')) {
      return parent::findById($id, $force_reload);
    } else {
      return FinancesTransactions::instance()->findById($id, $force_reload);
    }
  }

  function count($condition = null) {
    if (isset($this) && instance_of($this, 'FinancesTransactions')) {
      return parent::count($condition);
    } else {
      return FinancesTransactions::instance()->count($condition);
    }
  }

  function delete($condition = null) {
    if (isset($this) && instance_of($this, 'FinancesTransactions')) {
      return parent::delete($condition);
    } else {
      return FinancesTransactions::instance()->delete($condition);
    }
  }

  function paginate($arguments = null, $items_per_page = 10, $current_page = 1) {
    if (isset($this) && instance_of($this, 'FinancesTransactions')) {
      return parent::paginate($arguments, $items_per_page, $current_page);
    } else {
      return FinancesTransactions::instance()->paginate($arguments, $items_per_page, $current_page);
    }
  }

  function instance() {
    static $instance;

    if(!instance_of($instance, 'FinancesTransaction')) {
      $instance = new FinancesTransactions();
    }

    return $instance;
  }
}

?>