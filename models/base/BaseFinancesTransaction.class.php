<?php

abstract class BaseFinancesTransaction extends ProjectDataObject {

  function getObjectId() {
    return $this->getColumnValue('id');
  }

  function __call($method, $args) {
    if( preg_match('/(set|get)(_)?/', $method) ) {
      if(substr($method, 0, 3) == "get") {
	$col = substr(strtolower(preg_replace('([A-Z])', '_$0', $method)), 4);
	if( $col ) {
	  return $this->getColumnValue($col);
	}
      } elseif(substr($method, 0, 3) == "set" && count($args)) {
	$col = substr(strtolower(preg_replace('([A-Z])', '_$0', $method)), 4);
	if( $col ) {
	  return $this->setColumnValue($col, $args[0]);
	}
      }
    }

    return false;
  }

  function manager() {
    if (!($this->manager instanceof FinancesTransactions)) {
      $this->manager = FinancesTransactions::instance();
    }
    return $this->manager;
  }
}

?>