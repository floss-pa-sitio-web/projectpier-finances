<?php

class FinancesTransactions extends BaseFinancesTransactions {
  function queryTotal() {
    if(isset($this) && instance_of($this, 'FinancesTransactions')) {
      $tablename = $this->getTableName(true);
      $projectid = active_project()->getId();
      $row = DB::executeOne("SELECT SUM(amount) AS amount_total FROM $tablename WHERE project_id = $projectid");

      return (integer)array_var($row, 'amount_total', 0);
    } else {
      return FinancesTransactions::instance()->queryTotal();
    }
  }
}

?>