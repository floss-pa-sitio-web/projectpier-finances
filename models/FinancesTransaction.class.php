<?php

class FinancesTransaction extends BaseFinancesTransaction {
  function getProject() {
    if(is_null($this->project)) {
      $this->project = Projects::findProjectById($this->projectId);
    }

    return $this->project;
  }

  function canAdd(User $user, Project $project) {
    return $user->isAdministrator() || $user->isMemberOfOwnerCompany() || $user->isProjectUser(active_project());
  }

  function canEdit(User $user) {
    return $user->isAdministrator() || $user->isMemberOfOwnerCompany() || $user->isProjectUser(active_project());
  }

  function canDelete(User $user) {
    return $user->isAdministrator() || $user->isMemberOfOwnerCompany() || $user->isProjectUser(active_project());
  }

  function canView(User $user) {
    return $user->isAdministrator() || $user->isMemberOfOwnerCompany() || $user->isProjectUser(active_project());
  }  

  function getEditUrl() {
    return get_url('finances', 'edit_transaction', array('id' => $this->getId(), 'active_project' => $this->getProjectId()));
  }

  function getDeleteUrl() {
    return get_url('finances', 'delete_transaction', array('id' => $this->getId(), 'active_project' => $this->getProjectId()));
  }
}

?>
