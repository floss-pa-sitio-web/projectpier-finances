<?php

  return array(
	       'finances' => 'Finances',
	       'add transaction' => 'Add transaction',
	       'edit transaction' => 'Edit transaction',
	       'added transaction' => 'Transaction successfully added',
	       'editted transaction' => 'Transaction successfully editted',
	       'deleted transaction' => 'Transaction successfully deleted',
	       'issued on' => 'Issued On',
	       'amount' => 'Amount',
	       'finances transaction not found' => 'Transaction not found',
	       'log add financestransactions' => 'Transaction added',
	       'log edit financestransactions' => 'Transaction editted',
	       'log delete financestransactions' => 'Transaction deleted'
	       );

?>