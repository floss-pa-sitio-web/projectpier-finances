<?php

  /**
  * Finances Controller
  *
  * @http://www.floss-pa.org
  */
  class FinancesController extends ApplicationController {
  
    /**
    * Construct the FinancesController
    *
    * @access public
    * @param void
    * @return FinancesController
    */
    function __construct() {
      parent::__construct();
      prepare_company_website_controller($this, 'project_website');
    }
    
    function index() {
      $transactions_pagination = FinancesTransactions::paginate(array('order' => 'issued_on DESC'));

      tpl_assign('total', FinancesTransactions::queryTotal());
      tpl_assign('transactions', $transactions_pagination[0]);
    }
    
    function add_transaction(FinancesTransaction $transaction = null) {
      $this->setTemplate('add_transaction');
      $transaction_data = array_var($_POST, 'transaction');
      $action = ApplicationLogs::ACTION_EDIT;
      
      if($transaction == null) {
	$action = ApplicationLogs::ACTION_ADD;
	$transaction = new FinancesTransaction();
      }

      if(is_array($transaction_data)) {
	$transaction->setFromAttributes($transaction_data);
	$transaction->setCreatedById(logged_user()->getId());
	$transaction->setProjectId(active_project()->getId());

	try {
	  DB::beginWork();
	  $transaction->save();
	  ApplicationLogs::createLog($transaction, active_project(), $action);
	  DB::commit();

	  if($action == ApplicationLogs::ACTION_EDIT)
	    flash_success(lang('editted transaction'));
	  else
	    flash_success(lang('added transaction'));

	  $this->redirectTo('finances');
	} catch(Exception $e) {
	  DB::rollback();
	  tpl_assign('error', $e);
	}
      }

      tpl_assign('transaction', $transaction);
    }
    
    function edit_transaction() {
      $transaction = FinancesTransactions::findById(get_id());

      if (!FinancesTransaction::canEdit(logged_user())) {
        flash_error(lang('no access permissions'));
        $this->redirectTo('finances','index');
      }
      
      if (!($transaction instanceof FinancesTransaction)) {
        flash_error(lang('finances transaction not found'));
        $this->redirectTo('finances');
      }

      return $this->add_transaction($transaction);
    }
    
    function delete_transaction() {
      $transaction = FinancesTransactions::findById(get_id());

      if (!FinancesTransaction::canEdit(logged_user())) {
        flash_error(lang('no access permissions'));
        $this->redirectTo('finances','index');
      }
      
      if (!($transaction instanceof FinancesTransaction)) {
        flash_error(lang('finances transaction not found'));
        $this->redirectTo('finances');
      }

      try {
	DB::beginWork();
	$transaction->delete();
	ApplicationLogs::createLog($transaction, active_project(), ApplicationLogs::ACTION_DELETE);
	DB::commit();
	flash_success(lang('deleted transaction'));
	$this->redirectTo('finances');
      } catch(Exception $e) {
	DB::rollback();
	tpl_assign('error', $e);
      }
      
    }
  }

?>
