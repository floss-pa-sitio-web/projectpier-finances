<?php
  
  /**
  * All functions here are in the global scope so keep names unique by using
  *   the following pattern:
  *
  *   <name_of_plugin>_<pp_function_name>
  *   i.e. for the hook in 'add_dashboard_tab' use 'finances_add_dashboard_tab'
  */
  
  // add project tab
  add_action('add_project_tab', 'finances_add_project_tab');
  function finances_add_project_tab() {
    add_tabbed_navigation_item(
      'finances',
      'finances',
      get_url('finances', 'index')
    );
  }
  
  // overview page
  add_action('project_overview_page_actions','finances_project_overview_page_actions');
  function finances_project_overview_page_actions() {
    add_page_action(lang('add transaction'), get_url('finances', 'add_transaction'));
  }

  // my tasks dropdown
  add_action('my_tasks_dropdown','finances_my_tasks_dropdown');
  function finances_my_tasks_dropdown() {
    echo '<li class="header"><a href="'.get_url('finances', 'index').'">'.lang('finances').'</a></li>';
    echo '<li><a href="'.get_url('finances', 'add_transaction').'">'.lang('add transaction').'</a></li>';
  }
  
  /**
  * If you need an activation routine run from the admin panel
  *   use the following pattern for the function:
  *
  *   <name_of_plugin>_activate
  *
  *  This is good for creation of database tables etc.
  */
  function finances_activate() {
    $cs = 'character set '.config_option('character_set', 'utf8');
    $co = 'collate '.config_option('collation', 'utf8_unicode_ci');

    $sql = "CREATE TABLE IF NOT EXISTS `".TABLE_PREFIX."project_finances` (
        `id` int(10) unsigned NOT NULL auto_increment,
        `project_id` int(10) unsigned NOT NULL default '0',
        `description` TEXT $cs $co DEFAULT '',
        `amount` DECIMAL(10,2),
        `issued_on` datetime NOT NULL,
        `created_on` datetime NOT NULL,
        `created_by_id` int(10) unsigned default NULL,
        PRIMARY KEY  (`id`),
        KEY `issued_on` (`issued_on`),
        KEY `created_on` (`created_on`),
        KEY `project_id` (`project_id`)
      );";
    // create table
    DB::execute($sql);
  }

  /**
  * If you need an de-activation routine run from the admin panel
  *   use the following pattern for the function:
  *
  *   <name_of_plugin>_deactivate
  *
  *  This is good for deletion of database tables etc.
  */
  function finances_deactivate($purge=false) {
    // sample drop table
    if ($purge) {
      DB::execute("DROP TABLE IF EXISTS `".TABLE_PREFIX."project_finances`");
    }
  }
?>